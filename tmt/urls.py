"""tmt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from tmt import settings
from web import views, perms

urlpatterns = [
    url(r'^$', login_required(views.ArticleList.as_view()), name='newsfeed'),
    url(r'^(?P<model>\w+)/(?P<pk>\d+)$', login_required(views.Detail.as_view()), name='detail'),
    url(r'^admin/', admin.site.urls),
    url(r'^donation$', perms.permission_required()(views.DonationCreate.as_view()), name='donation'),
    url(r'^group$', perms.permission_required()(views.GroupCreate.as_view()), name='group'),
    url(r'^login$', views.login_view, name='login'),
    url(r'^logout$', views.logout_view, name='logout'),
    url(r'^select/(?P<tab>.*)$', login_required(views.SelectList.as_view()), name='select'),
    url(r'^signup$', views.Signup.as_view(), name='signup'),
    url(r'^write$', perms.permission_required()(views.ArticleCreate.as_view()), name='write'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)