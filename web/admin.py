from django.contrib import admin

from web.forms import *
from web.models import *


class ImageInline(admin.TabularInline):
    model = Group.image.through
    extra = 0


class ClientAdmin(admin.ModelAdmin):
    list_display = ['__str__']


class GroupAdmin(admin.ModelAdmin):
    list_display = ['__str__']
    form = GroupForm
    inlines = [ImageInline, ]
    # exclude = ['image', ]


admin.site.register(Article)
admin.site.register(Category)
admin.site.register(Donation)
admin.site.register(Client, ClientAdmin)
admin.site.register(Group, GroupAdmin)
