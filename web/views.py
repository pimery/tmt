from django.apps import apps
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from web.forms import *


from django.views.generic import *


class ArticleCreate(CreateView):
    model = Article
    fields = '__all__'
    template_name = 'create_article.html'
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.writer = Client.objects.get(user=request.user)
            article.save()
            return redirect('/article/' + article.id)
        return render(self.template_name, {'form': form})


class ArticleList(ListView):
    model = Article
    template_name = 'index.html'
    queryset = Article.objects.all().order_by('-written')

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super(ArticleList, self).get(request, args, kwargs)
        else:
            return redirect('login')

    def get_context_data(self, **kwargs):
        context = super(ArticleList, self).get_context_data(**kwargs)
        context['menu'] = 'n'
        return context


class Detail(DetailView):
    def get(self, request, *args, **kwargs):
        self.model = apps.get_model('web', self.kwargs['model'])
        self.template_name = 'detail_%s.html' % self.kwargs['model']
        return super(Detail, self).get(request, args, kwargs)


class DonationCreate(CreateView):
    model = Donation
    template_name = 'create_donation.html'
    form_class = DonationForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            donation = form.save(commit=False)
            donation.writer = Client.objects.get(user=request.user)
            donation.save()
            return redirect('/donation/%d' % donation.id)
        return render(self.template_name, {'form': form})


class GroupCreate(CreateView):
    model = Group
    template_name = 'create_group.html'
    form_class = GroupForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            group = form.save()
            donation = Donation(name=group.name, group=group)
            donation.category = Category.objects.get(index=0)
            donation.writer = Client.objects.get(user=request.user)
            donation.save()
        return redirect(self.success_url)


class SelectList(ListView):
    category_id = -1
    template_name = 'select.html'

    def get(self, request, *args, **kwargs):
        self.category_id = Category.objects.get(name=kwargs['tab']).id
        return super(SelectList, self).get(request, args, kwargs)

    def get_queryset(self):
        try:
            return Donation.objects.filter(category__id=self.category_id)
        except:
            return Group.objects.all()

    def get_context_data(self, **kwargs):
        context = super(SelectList, self).get_context_data(**kwargs)
        context['tabs'] = get_tabs(self.category_id)
        context['menu'] = 's'
        return context


class Signup(CreateView):
    template_name = 'signup.html'
    form_class = SignupForm
    success_url = '/'


def login_view(request):
    logout(request)
    redirect_to = ''
    message = ''
    if 'next' in request.GET:
        redirect_to = request.GET['next']
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        # redirect_to = request.POST['next']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if redirect_to != '':
                    return HttpResponseRedirect(redirect_to)
                return redirect('newsfeed')
        else:
            message = '잘못된 계정입니다'
    return render(request, 'login.html', {'next': redirect_to, 'mes': message})


def logout_view(request):
    logout(request)
    if 'next' in request.GET:
        redirect_to = request.GET['next']
        return HttpResponseRedirect(redirect_to)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def get_tabs(id):
    tabs = []
    for cate in Category.objects.all().order_by('index'):
        item = {'name': cate.name, 'cur': 0}
        if cate.id == id:
            item['cur'] = 1
        tabs.append(item)
    return tabs
