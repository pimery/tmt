from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied

from web.models import Client


def permission_required(login_url=None):
    def check_perms(user):
        if user.is_active and (user.is_staff or user.is_superuser):
            return True
        try:
            client = Client.objects.get(user=user)
        except:
            raise PermissionDenied
        if client.is_staff():
            return True
        raise PermissionDenied
    return user_passes_test(check_perms, login_url=login_url)
