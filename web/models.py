from django.contrib.auth.models import User
from django.db import models


def file_path(self, filename):
    url = '%s/%s' % (self.path, filename)
    return url


class Image(models.Model):
    path = models.CharField(default='', max_length=20)
    img = models.ImageField(upload_to=file_path)
    content = models.CharField(verbose_name='설명', max_length=100)


class Group(models.Model):
    name = models.CharField(verbose_name='기부처 이름', max_length=30)
    content = models.TextField(verbose_name='기부처에 대한 설명')
    staff = models.ManyToManyField('Client', verbose_name='스탭', blank=True)
    image = models.ManyToManyField(Image, verbose_name='사진', blank=True, related_name='groups')

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(verbose_name='기부 분류', max_length=30)
    index = models.IntegerField(default=0, unique=True)

    def __str__(self):
        return self.name


class Donation(models.Model):
    name = models.CharField(verbose_name='이름', max_length=100)
    group = models.ForeignKey(Group, verbose_name='기부처')
    category = models.ForeignKey(Category, verbose_name='기부 분류')
    start = models.DateTimeField(verbose_name='시작 날짜', auto_now=True)
    end = models.DateTimeField(verbose_name='마감 날짜', null=True, blank=True)
    writer = models.ForeignKey('Client', verbose_name='작성자', editable=False)

    def __str__(self):
        return self.name

    def is_group(self):
        if self.end:
            return False
        return True


class Client(models.Model):
    user = models.ForeignKey(User)
    rank = models.IntegerField(verbose_name='등급', default=0)
    follow = models.ManyToManyField(Group, verbose_name='팔로우', blank=True)
    donate = models.ManyToManyField(Donation, verbose_name='기부 내역', blank=True)

    def __str__(self):
        return self.user.username

    def is_staff(self):
        return self.rank > 0


class Article(models.Model):
    title = models.CharField(verbose_name='제목', max_length=100)
    cover = models.ImageField(verbose_name='커버 사진', upload_to='article', blank=True)
    content = models.TextField(verbose_name='내용')
    like = models.IntegerField(verbose_name='좋아요', default=0, editable=False)
    donation = models.ManyToManyField(Donation, verbose_name='관련된 기부', blank=True)
    writer = models.ForeignKey(Client, verbose_name='작성자', editable=False)
    written = models.DateTimeField(auto_now_add=True, editable=False)

    def __str__(self):
        return self.title
